import React, { useState, useEffect, useReducer, useRef, useMemo } from 'react';
import axios from 'axios';
import List from './List';
import { useFormInput } from '../hooks/forms'; 




const todo = props => {

    const [inputIsValid, setInputIsValid] = useState(false);

    //const [todoName, setTodoName] = useState('');
    //const [submittedTodo, setSubmittedTodo] = useState(null);
    //const [todoList, setTodoList] = useState([]);

    const todoInputRef = useRef();
    const todoInput = useFormInput();


    const todoListReducer = (state, action) => {

        
        switch (action.type) {
            case 'ADD': 
                return state.concat(action.payload);
            case 'SET':
                return action.payload;
            case 'REMOVE':
                return state.filter((todo) => {
                    console.log(todo);
                    return(
                        todo.id !== action.payload
                    )
                });
            default:
                return state;
        }
    };

    const [todoList, dispatch] = useReducer(todoListReducer, []); 


    //const [todoState, setTodoState] = useState({userInput: '', todoList: []});

    /*
    useEffect(() => {

        async function fetchData() {
            try {
                const response = await axios.get('https://react-hooks-67b1b.firebaseio.com/todos.json');
                console.log(response);
            }catch(error){
                console.log(error);
            }
        }

        fetchData();
    });
    */

    useEffect(() => {
        console.log('useeffect');
        axios.get('https://react-hooks-67b1b.firebaseio.com/todos.json')
            .then((result) => {
                //console.log(result.data);
                const todoData = result.data;
                //console.log(todoData); 

                const todos = [];
                for(const key in todoData){
                    todos.push({id: key, name: todoData[key].name});
                }
                //console.log(todos);
                dispatch({type: 'SET', payload: todos });
            })
            .catch((error) => console.log(error));
    }, []);


    /*
    useEffect(() => {

        console.log('inside userEffect 2');

        if(submittedTodo){
            dispatch({type: 'ADD', payload: submittedTodo });

        }
    }, [submittedTodo]);
    */



    const mouseMoveHandler = event => {
        //console.log(event.clientX, event.clientY);
    };




    useEffect(() => {
        console.log('useeffect');
        document.addEventListener('mousemove', mouseMoveHandler);
        return () => {
            console.log('Cleanup MouseListener');
            document.removeEventListener('mousemove', mouseMoveHandler);
        }
    }, []);

    /*
    const inputChangeHandler = (event) => {
        /*
        setTodoState({
            userInput: event.target.value,
            todoList: todoState.todoList
        });
        */
       //setTodoName(event.target.value);
    //}
    


    const todoAddHandler = async () => {
        /*
        setTodoState({
            userInput: todoState.userInput,
            todoList: todoState.todoList.concat(todoState.userInput)
        });
        */

        const todoName = todoInput.value;
       

       try{
           const response = await axios.post('https://react-hooks-67b1b.firebaseio.com/todos.json', {name: todoName});
           
           setTimeout(() => {
               console.log('inside timeout');
               const todoItem = { id: response.data.name, name: todoName }
               dispatch({type: 'ADD', payload: todoItem});
           }, 2000);
           //console.log(response);
       }
       catch(error){
            console.log(error);
       }
       
    }

    const todoRemoveHandler = async todoId => {
        try{
            await axios.delete(`https://react-hooks-67b1b.firebaseio.com/todos/${todoId}.json`);
            dispatch({type: 'REMOVE', payload: todoId});
        }catch(error){
            console.log(error);
        }
    } 

    const inputValidationHandler = event => {
        if(event.target.value.trim() === '') {
            setInputIsValid(false);
        }else{
            setInputIsValid(true);
        }
    }

    return(
        <React.Fragment>
            <input 
                type="text" 
                placeholder="Todo" 
                //onChange={inputChangeHandler} 
                //value={todoState.userInput} 
                //value={todoName} 
                onChange={todoInput.onChange}
                value={todoInput.value}
                style={{backgroundColor: todoInput.validity ? 'transparent' : 'red'}}
            />

            <button type="button" onClick={todoAddHandler}>Add</button>
            {useMemo(() => <List items={todoList} onClick={todoRemoveHandler} />, [todoList])}

        </React.Fragment>
    )

};

export default todo;